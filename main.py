#!/usr/bin/env python
# -*- coding: UTF-8 -*-
'''
author: Adriano Vieira <adriano.svieira at google.com>
'''

import logging
import yaml
from multiprocessing import Process
from beats import beat, beat_t


if __name__ == '__main__':

    try:
        CONFIG = yaml.safe_load(open('config/config.yml'))
    except Exception as error:
        logging.error('Erro ao tentar abrir configurações:\n %s', error)
        raise

    logging.basicConfig(level=CONFIG['logging'])

    try:
        DIC = yaml.safe_load(open('config/dictionary.yml'))
    except Exception as error:
        logging.error('Erro ao tentar abrir dicionário:\n %s', error)
        raise

    logging.warning('inicia multiprocessing')
    for beat_id in DIC['beat']:
        logging.info(beat_id)

        # multiprocessing class
        p = Process(target=beat.Pid, args=(CONFIG, beat_id,))
        p.start()
        p.join()
    logging.warning('finaliza multiprocessing')

    print("\n\n\n")

    logging.warning('inicia threading')
    for beat_id in DIC['beat']:
        logging.info(beat_id)
        # threading class
        b = beat_t.PidT(CONFIG, beat_id)
        b.start()
        b.join()

    logging.warning('finaliza multiprocessing')
