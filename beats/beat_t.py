# -*- coding: UTF-8 -*-
'''
Application: StreamingAudit
Module: Validate
description: Validate check if a captured image is a brand
author: Adriano Vieira <adriano.svieira at google.com>
character encoding: UTF-8
'''

from threading import Thread
import os
import time
import logging
from datetime import datetime
from elasticsearch import Elasticsearch
import random


class PidT(Thread):
    def __init__(self, config, beat_id):
        Thread.__init__(self)
        self.CONFIG = config
        self.beat_id = beat_id

        logging.basicConfig(level=self.CONFIG['logging'])

        self.rand_sleep =  random.randint(0, self.CONFIG['beat']['sleep_time'])

        # Elasticsearch
        if self.CONFIG['db']['elk']['enabled']:
            self.elk_host = self.CONFIG['db']['elk']['host']
            self.elk_host_port = self.CONFIG['db']['elk']['port']
            self.index_name = self.CONFIG['db']['elk']['index_name']
            try:
                self.elk_conn = Elasticsearch([{'host': self.elk_host, 'port': self.elk_host_port}])
                document={ 'id_beats': 'connection-test',
                           'name': 'Teste de conexão',
                           'timestamp': datetime.utcnow(),
                           'beat': 1, }
                self.elk_conn.index(index=self.index_name,
                                     doc_type='beats',
                                     id=1,
                                     body=document)
            except Exception as e:
                raise
            finally:
                self.elk_conn.delete(index=self.index_name, id=1)

    def run(self):
        document={ 'id_beats': os.getpid(),
                   'id_ppid': os.getppid(),
                   'timestamp': datetime.utcnow(),
                   'randint': self.rand_sleep,
                   'beat': self.beat_id, }

        if self.CONFIG['db']['elk']['enabled']:
            self.elk_conn.index(index=self.index_name,
                             doc_type='beats',
                             body=document)

        logging.info('Sleeping [%s] on beat_t id [%s] and pid [%s]'
                      % (str(self.rand_sleep), self.beat_id, os.getpid()))
        time.sleep(self.rand_sleep)
        logging.warning('Slept [%s] on beat_t id [%s] and pid [%s]/ppid [%s]'
                      % (str(self.rand_sleep), self.beat_id, os.getpid(), os.getppid()))
